/*
FIX SAVE DIR ISSUES!!!!!!!!!
*/

package main

import (
	"bufio"
	"fmt"
	"image"
	"io/ioutil"
	"os"
	"strconv"
	"strings"
	"time"

	options "gitlab.com/okotech/loadoptions"
	"gitlab.com/okotek/backstackimagehandler"
	"gitlab.com/okotek/okolog"
	okonet "gitlab.com/okotek/okonet"
	types "gitlab.com/okotek/okotypes"
	"gocv.io/x/gocv"
)

var opts types.ConfigOptions

func main() {

	//defer profile.Start(profile.MemProfile).Stop()
	opts = options.Load()
	opts.BackStackStorageDirectory = "../users"
	opts.SinceRewriteDelay = 0

	stageOneProcessor := make(chan types.Frame)
	stageTwoProcessor := make(chan types.Frame)
	//newImageEmailSendoff := make(chan types.Frame)

	//okolog.LoggerIDInit(opts)
	//opts.InstanceIdentifier = okolog.InitStruct.Identifier

	go backstackimagehandler.ImgHandler(stageOneProcessor)
	go logImgNames(stageOneProcessor, stageTwoProcessor)
	go okonet.ReceiverHandler(stageTwoProcessor, ":8083")
	//go okonet.SendoffHandler(newImageEmailSendoff, ":9002")
	//TEST COMMENT
	scanner := bufio.NewScanner(os.Stdin)
	for scanner.Scan() {
		fmt.Println(scanner.Text())
	}

}

//Log names and detect duplicates
func logImgNames(input chan types.Frame, output chan types.Frame) {

	var nameStore map[string]int
	for frm := range input {

		if _, ok := nameStore[frm.ImgName]; !ok {
			nameStore[frm.ImgName] = 1
		} else {
			nameStore[frm.ImgName] = nameStore[frm.ImgName] + 1
			okolog.MessageLogger(fmt.Sprintf("Found matching image names: %s. This is the %d time this has happened.", frm.ImgName, nameStore[frm.ImgName]), "LOG")
		}

	}
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//EVERYTHING BELOW THIS IS TO BE DEPRECIATED
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//SURE YOU WANT TO BE BELOW THIS???

//WHAT ARE YOU DOING?

//'ERE BE DRAGONS

//REALLY MAN?

//OKAY....

//HERE IT IS...
/*
	Having some serious issues getting the primary imgHandler to work, so
	this one has been implemented tempoarily in its place until we can get
	it back up and running.
*/

func _imgHandler(input chan types.Frame) {
	for iter := range input {
		fmt.Println("Created! 🙌")
		img := types.CreateMatFromFrameSimp(iter)
		tnow := time.Now().Unix()
		tString := strconv.Itoa(int(tnow))
		gocv.IMWrite("/home/user1/Dev/okokit_x/"+tString+".jpg", img)
	}
}

func __imgHandler(input chan types.Frame) {

	okolog.MessageLogger("imgHandler is being used.", "regular")
	fmt.Println("imgHandler is being used.")

	//time.Sleep(time.Second * 10)

	var store []types.Frame
	var storep *[]types.Frame = &store
	var sinceRewrite float64 = 1 - 1 // This equals zero

	lastCheck := time.Now()

	/*
		Four goroutines to 1) check if a directory exists and create if
		if not, 2) update the hashfile if the directory already exists
		in case of a change and 3) & 4) save full size and thumbnail images
		to that directory.
	*/
	for iter := range input {
		fmt.Println(opts.BackStackStorageDirectory + "/" + iter.UserName)
		fmt.Println(iter.ImgName, "==============================================================")
		//okolog.MessageLogger("New image being written now.")

		//Check if directory exists and create it if not.
		go func() {
			if _, err := os.Stat(opts.BackStackStorageDirectory + "/" + iter.UserName); os.IsNotExist(err) {

				fmt.Println("Holy shit, the folder is missing!")
				err := os.Mkdir(".."+opts.BackStackStorageDirectory+"/"+iter.UserName, 0777)
				fmt.Println("ERROR:", err, "Shutting the fuck down!")
				os.Exit(1)

				_, osCreateFileError := os.Create(opts.BackStackStorageDirectory + "/" + iter.UserName + "/hashFile.txt")
				if osCreateFileError != nil {
					//okolog.MessageLogger(fmt.Sprintf("Issue creating file in backstack. Error message: %s \n", osCreateFileError))
				}
				ioutil.WriteFile(".."+opts.BackStackStorageDirectory+"/"+iter.UserName+"/hashFile.txt", []byte(iter.UserPass), 0777)

			}
		}()

		//Re-write the hash file every so often so that password changes get pushed to the hash file
		go func() {
			sinceRewrite = time.Now().Sub(lastCheck).Seconds()
			//okolog.MessageLogger(fmt.Sprintf("\"Since Rewrite\" value is : %f\n", sinceRewrite))
			if sinceRewrite > opts.SinceRewriteDelay {
				ioutil.WriteFile(opts.BackStackStorageDirectory+"/"+iter.UserName+"/hashFile.txt", []byte(iter.UserPass), 0777)
			}
			lastCheck = time.Now()
		}()

		//Write images to user directory
		go func() {

			var localTagList []string
			fmt.Println("Size fo image: ", len(iter.ImDat))

			*storep = append(*storep, iter)
			img, err := gocv.NewMatFromBytes(iter.Height, iter.Width, iter.Type, iter.ImDat)
			if err != nil {
				fmt.Println("Could not create new gocv mat.\nGOCV Error Message:", err)
				os.Exit(1)
			}

			for _, tag := range iter.ClassifierTags {
				if len(tag.Rects) > 0 {
					localTagList = append(localTagList, tag.ClassName)
				}
			}

			tagString := strings.Join(localTagList, "_")
			tagString = strings.Replace(tagString, ".", "", -1)
			tagString = strings.Replace(tagString, "xml", "", -1)
			tagString = strings.Replace(tagString, "classifiers/", "", -1)
			tagString = strings.Replace(tagString, "/", "", -1)

			writename := "../" + opts.BackStackStorageDirectory + "/" + iter.UserName + "/oko" + tagString + "|" + strconv.Itoa(int(iter.ImTime.UnixNano())) + ".jpg"

			fmt.Print("\n\nWriting " + writename)
			gocv.IMWrite(writename, img)

		}()

		//Write thumbnails to user directory
		go func() {

			var localTagList []string
			tinyMat := gocv.NewMatWithSize(iter.Height, iter.Width, iter.Type)

			*storep = append(*storep, iter)
			img, _ := gocv.NewMatFromBytes(iter.Height, iter.Width, iter.Type, iter.ImDat)

			for _, tag := range iter.ClassifierTags {
				if len(tag.Rects) > 0 {
					localTagList = append(localTagList, tag.ClassName)
				}
			}

			tagString := strings.Join(localTagList, "_")
			tagString = strings.Replace(tagString, ".", "", -1)
			tagString = strings.Replace(tagString, "xml", "", -1)
			tagString = strings.Replace(tagString, "classifiers/", "", -1)

			resizePoint := image.Point{Y: img.Rows() / 4, X: img.Cols() / 4}

			gocv.Resize(img, &tinyMat, resizePoint, 0, 0, 0)

			gocv.IMWrite(opts.BackStackStorageDirectory+"/"+iter.UserName+"/oko"+tagString+"|"+strconv.Itoa(int(iter.ImTime.UnixNano()))+".tiny.jpg", tinyMat)

		}()
	}
}
