module gitlab.com/okotek/backstack

go 1.17

require (
	gitlab.com/okotech/loadoptions v0.0.0-20220331051017-d846c01d796e
	gitlab.com/okotek/backstackimagehandler v0.0.0-20220707010935-79dba2df2be0
	gitlab.com/okotek/okolog v0.0.0-20220408231632-406a0d298432
	gitlab.com/okotek/okonet v0.0.0-20220331051130-c496df21780f
	gitlab.com/okotek/okotypes v0.0.0-20220610031138-9b417e09954f
	//gitlab.com/okotek/backstackimagehandler
	gocv.io/x/gocv v0.31.0
)

require (
	github.com/go-sql-driver/mysql v1.6.0 // indirect
	gitlab.com/okotek/sec v0.0.0-20220326221119-e10389566a6b // indirect
	golang.org/x/crypto v0.0.0-20220331220935-ae2d96664a29 // indirect
)

replace gitlab.com/okotek/okonet => ../okonet

replace gitlab.com/okotech/okotypes => ../okotypes

replace gitlab.com/okotech/okolog => ../okolog

replace gitlab.com/okotech/loadoptions => ../loadoptions/

replace gitlab.com/okotech/backstackimagehandler => ../backstackimagehandler

replace gitlab.com/okotek/okolog => ../okolog
